package edu.udmercy.volleyrequest

import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import java.nio.charset.Charset


class MainActivity : AppCompatActivity() {
    private var textView: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getParking()
    }

    private fun getParking() {
        Log.d("API", "Calling getParking...")

        val url = "https://www.parkofon.com/api/udm/getparking"
        val requestBody = "bounds=40.696718 -73.975905 40.694239 -73.980668" +
                "&timezone=America/New_York" +
                "&hourlimit=0" +
                "&hourrate=10" +
                "&handicapped=0" +
                "&garage=0" +
                "&username=1248555" +
                "&token=UiwK%23X7sjxGmZG63x%3jqVPejP%23@9hmm"

        try {
            Volley.newRequestQueue(this).add(object :
                StringRequest(Method.POST, url,

                    Response.Listener { response ->
                        Log.d("API", "Status: Success\n $response")
                    },

                    Response.ErrorListener { error ->
                        Log.d("API", "Status: Fail\n $error")
                    }
                ) {
                override fun getBody(): ByteArray {
                    return requestBody.toByteArray(Charset.defaultCharset())
                }
            })
        } catch (ex: Exception) {
            Log.e("API", "An error occurred -> ${ex.message}")
        }
    }
}
